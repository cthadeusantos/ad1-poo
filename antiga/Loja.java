// Loja.java
// AD1 - 2019.02
// Carlos Thadeu Santos - 17113050228

// ATENCAO: compilar com
//
// javac Loja.java -encoding UTF-8
//
// para a acentuação aparecer corretamente

// MAIN CLASS
class Loja {
	public static void main(String[] args) {
		Simples produto1 = new Simples();
		produto1.setNome("Pincel");
		produto1.setPreco(10.3F);
		System.out.println(produto1);
		
		Simples produto2 = new Simples();
		produto2.setNome("Rolo de tinta");
		produto2.setPreco(12.4F);
		System.out.println(produto2);

		Simples produto3 = new Simples();
		produto3.setNome("Tinta ciano");
		produto3.setPreco(3.1F);
		System.out.println(produto3);

		Simples produto4 = new Simples();
		produto4.setNome("Tinta magenta");
		produto4.setPreco(2.8F);
		System.out.println(produto4);

		Composto produto5 = new Composto();
		produto5.setNome("Tinta mista");
		produto5.addItem(produto3);
		produto5.addItem(produto4);
		produto5.exibeItens();

		Carrinho carrinho1 = new Carrinho();
		carrinho1.carregaCarrinho(produto2, 10);
		carrinho1.carregaCarrinho(produto1, 20);
		carrinho1.carregaCarrinho(produto5, 24);
		carrinho1.orcamento();
		carrinho1.descarregaCarrinho(1); // Remove o item 1 do carrinho
		carrinho1.orcamento();
	}
}

// Auxiliary Class
// Métodos removidos da classe abstrata
abstract class Produto {
	private static int proximo = 1;
	protected int codigo; // código do produto
	protected String nome;
	// Construtor
	Produto(){
		this.codigo = proximo++;
	}
	// Métodos definidos na classe abstrata
	// contém apenas o nome e o retorno da funcao
	//sem nenhuma implementação
	//
	// Define nome do produto
	public String setNome(String nome){
		this.nome = nome;
		return this.nome;
	}
	// Retorna nome do produto
	public String getNome(){
		return this.nome;
	}
	// Retorna código do produto
	public int getCodigo() {	
		return this.codigo;
	}

	//public void exibePreco() {
	//	System.out.println(getPreco());
	//}
	//public void exibeNome() {
//		System.out.println(getNome());
//	}
//	public void exibeCodigo() {
//		System.out.println(getCodigo());
//	}

	// Retorna o preco
	abstract float getPreco();

	// Retorna codigo, nome e preco
	@Override
	public String toString(){
			return "Código: " + getCodigo() + " Produto: " + getNome() + " Preço: " + getPreco();
	}
} // end of public class Produto

class Simples extends Produto {
	// Aqui eu estava errando colocando private static
	// isso forcava preco a assumir o ultimo valor entrado em todas as instancias
	// foi so retirar que voltou ao normal ( entender o porquê disto acontecer )
	protected float preco;
	// Construtor
	public Simples() {
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
	}
	// Define preco
	public float setPreco(float preco){
		this.preco = preco;
		return this.preco;
	}
	// Recupera preco
	public float getPreco() {
		return this.preco;
	}


} // End of Simples Class

class Composto extends Produto {
	private final int MAX_SIZE_ITENS = 100;
	protected float preco;
	private Simples[] itens;
	protected int itens_composicao;

	public Composto(){
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
		itens = new Simples[MAX_SIZE_ITENS];
		itens_composicao = 0;
	}
	public void exibeItens(){
		System.out.println("Código: " + this.getCodigo() + " Produto:" + this.nome + " Valor: " + this.sumPrice());
		System.out.println("    Composicao");
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			System.out.println("    Item: " + (counter+1) + " " + this.itens[counter].getNome());
		}
	}
	// Adiciona item ao produto composto
	public void addItem(Simples value) {
		if (this.itens_composicao < MAX_SIZE_ITENS) {
			itens[this.itens_composicao] = value;
			this.itens_composicao++;
		}
	}
	// Remove item do produto composto
	public void delItem(int value){
		value--;
		if ((value < 0) || (value > this.itens_composicao)) {
			System.out.println("Não foi possível remover item da composicao");
			return;
		}
		System.out.println("Removendo: " + this.itens[value].nome);
		if (value == this.itens_composicao) {
			this.itens[this.itens_composicao] = null;
			this.itens_composicao--;
		} else {
			this.itens[value] = this.itens[this.itens_composicao-1];
			this.itens[this.itens_composicao-1] = null;
			this.itens_composicao--;
		}		
	}
	// Recupera preco
	public float getPreco() {
		this.preco = 0F;
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			if (this.itens_composicao < MAX_SIZE_ITENS) {
				this.preco += itens[counter].getPreco();
			}
		}
		return this.preco;
	}
	// totaliza composicao
	public float sumPrice(){
		float acumulador = 0F;
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			acumulador += this.itens[counter].getPreco();
		}
		return acumulador;		
	}

} // End of Composto class

//////////////////////////////
//							//
//     CLASS CARRINHO		//
//							//
//////////////////////////////
class Carrinho {
	//private static int codigo = 1;
	private final int MAX_SIZE_CARRINHO = 100;
	private Produto[] produtos;
	private float[] quantidade;
	protected int itens_carrinho;

	// Na falta de nomes melhores
	// adiciona algo ao carrinho ==> carregaCarrinho(codigo,quantidade)
	//
	// Remove algo do carrinho ==> descarregaCarrinho(codigo)
	//
	//
	// Construtor
	//
	public Carrinho(){
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
		produtos = new Produto[MAX_SIZE_CARRINHO];
		quantidade = new float[MAX_SIZE_CARRINHO];
		itens_carrinho = 0;		
	}
	// Adiciona produto ao carrinho
	public void carregaCarrinho(Produto codigoItem, int quantidadex){
		// Se não existe quantidade de produtoa ser adicionada, NÃO adiciona ao carrinho
		if (quantidadex == 0) {
			return;
		}
		if (this.itens_carrinho < MAX_SIZE_CARRINHO){
			produtos[this.itens_carrinho] = codigoItem;
			quantidade[this.itens_carrinho] = quantidadex;
			this.itens_carrinho++;
		}
	}
	// Remove produto do carrinho
	public void descarregaCarrinho(int item){
		item--;
		if ((item < 0) || (item > this.itens_carrinho)) {
			System.out.println("Não foi possível remover item do carrinho");
			return;
		}
		System.out.println("Removendo: " + this.produtos[item].nome);
		if (item == this.itens_carrinho) {
			this.produtos[this.itens_carrinho] = null;
			this.quantidade[this.itens_carrinho] = 0;
			this.itens_carrinho--;
		} else {
			this.produtos[item] = this.produtos[this.itens_carrinho-1];
			this.quantidade[item] = this.quantidade[this.itens_carrinho-1];
			this.produtos[this.itens_carrinho-1] = null;
			this.quantidade[this.itens_carrinho-1] = 0;
			this.itens_carrinho--;
		}
	}
	public void exibeConteudoCarrinho(){
		System.out.println("Pacotes dentro do carrinho");
		for (int counter = 0; counter < this.itens_carrinho; counter++) {
			System.out.println("Item: " + (counter+1) +  " " + this.produtos[counter].nome + "R$ " + this.produtos[counter].getPreco() + " Qtde: " + this.quantidade[counter]);
		}
	}
	//////////////////////////////////////////
	// ORCAMENTO PEDIDO NA AD1              // 
	// implementado como método do carrinho //
	//////////////////////////////////////////
	public float orcamento() {
		float acumulador = 0;
		System.out.println("");
		this.exibeConteudoCarrinho();
		for (int counter = 0; counter < this.itens_carrinho; counter++) {
			acumulador += this.produtos[counter].getPreco() * this.quantidade[counter];
		}
		System.out.println("ORÇAMENTO ATÉ O MOMENTO: R$ "+ acumulador);
		return acumulador;
	}

} // end of carrinho class