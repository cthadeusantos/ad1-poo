//////////////////////////////
//							//
//     CLASS CARRINHO		//
//			v.0.1.1			//
//////////////////////////////
class Carrinho {
	//private static int codigo = 1;
	private final int MAX_SIZE_CARRINHO = 100;
	private Produto[] produtos;
	private float[] quantidade;
	protected int itens_carrinho;

	// Na falta de nomes melhores
	// adiciona algo ao carrinho ==> addProduto(codigo,quantidade)
	//
	// Remove algo do carrinho ==> delProduto(codigo)
	//
	// Exibe orcamento ==> orcamento()
	//
	// Construtor
	//
	public Carrinho(){
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
		produtos = new Produto[MAX_SIZE_CARRINHO];
		quantidade = new float[MAX_SIZE_CARRINHO];
		itens_carrinho = 0;		
	}
	// Adiciona produto ao carrinho
	public void addProduto(Produto codigoItem, int quantidadex){
		// Se não existe quantidade de produtoa ser adicionada, NÃO adiciona ao carrinho
		if (quantidadex == 0) {
			return;
		}
		if (this.itens_carrinho < MAX_SIZE_CARRINHO){
			produtos[this.itens_carrinho] = codigoItem;
			quantidade[this.itens_carrinho] = quantidadex;
			System.out.println("Adicionando " + this.produtos[itens_carrinho].nome + " ao carrinho.");
			this.itens_carrinho++;
		}
	}
	// Remove produto do carrinho
	public void delProduto(int item){
		item--;
		if ((item < 0) || (item > this.itens_carrinho)) {
			System.out.println("Não foi possível remover item do carrinho");
			return;
		}
		System.out.println("Removendo " + this.produtos[item].nome + " do carrinho.");
		if (item == this.itens_carrinho) {
			this.produtos[this.itens_carrinho] = null;
			this.quantidade[this.itens_carrinho] = 0;
			this.itens_carrinho--;
		} else {
			this.produtos[item] = this.produtos[this.itens_carrinho-1];
			this.quantidade[item] = this.quantidade[this.itens_carrinho-1];
			this.produtos[this.itens_carrinho-1] = null;
			this.quantidade[this.itens_carrinho-1] = 0;
			this.itens_carrinho--;
		}
	}
	private void makeSpace(int number){
		for(int i = 0; i < number;i++){ System.out.print(" "); }
	}
	private void exibeConteudoCarrinho(){
		int numLetras;
		//System.out.println("Pacotes dentro do carrinho");
		for (int counter = 0; counter < this.itens_carrinho; counter++) {
			System.out.printf("Item: %d", counter+1);
			System.out.printf("\t%s", this.produtos[counter].nome);
			numLetras = this.produtos[counter].nome.length();
			this.makeSpace(35-numLetras);
			System.out.printf("\tR$%3.2f", this.produtos[counter].getPreco());
			System.out.printf("\tQtde: %4.1f\n", this.quantidade[counter]);			
			//System.out.printf("Item:\t%d\t\t%s\t\t\tR$%3.2f\t\t%3.1\n", (counter+1), this.produtos[counter].nome,this.produtos[counter].getPreco(),this.quantidade[counter]);
			//System.out.println("Item: " + (counter+1) +  " " + this.produtos[counter].nome + " R$ " + this.produtos[counter].getPreco() + " Qtde: " + this.quantidade[counter]);
		}
	}
	//////////////////////////////////////////
	// ORCAMENTO PEDIDO NA AD1              // 
	// implementado como método do carrinho //
	//////////////////////////////////////////
	public float orcamento() {
		String lines = new String("-------------");
		float acumulador = 0;
		System.out.println("\n" + lines + "ORCAMENTO" + lines + "\n");
		this.exibeConteudoCarrinho();
		for (int counter = 0; counter < this.itens_carrinho; counter++) {
			acumulador += this.produtos[counter].getPreco() * this.quantidade[counter];
		}
		this.makeSpace(40);
		System.out.printf("Total: R$ %s\n", String.format("%4.2f", acumulador));
		System.out.println(lines + lines + lines);
		return acumulador;
	}

} // end of carrinho class