// Loja.java
// AD1 - 2019.02
// Carlos Thadeu Santos - 17113050228

// ATENCAO: compilar com
//
// javac Loja.java -encoding UTF-8
//
// para a acentuação aparecer corretamente


// MAIN CLASS
class Teste {
	public static void main(String[] args) {
		Simples produto1 = new Simples();
		produto1.setNome("Pincel");
		produto1.setPreco(10.3F);
		System.out.println(produto1);
		
		Simples produto2 = new Simples();
		produto2.setNome("Rolo de tinta");
		produto2.setPreco(12.4F);
		System.out.println(produto2);

		Simples produto3 = new Simples();
		produto3.setNome("Tinta ciano");
		produto3.setPreco(3.1F);
		System.out.println(produto3);

		Simples produto4 = new Simples();
		produto4.setNome("Tinta magenta");
		produto4.setPreco(2.8F);
		System.out.println(produto4);

		Composto produto5 = new Composto();
		produto5.setNome("Tinta mista");
		produto5.addItem(produto3);
		produto5.addItem(produto4);
		produto5.exibeItens();

		Simples produto6 = new Simples();
		produto6.setNome("Bandeja de pintura 20cm");
		produto6.setPreco(12.83F);
		System.out.println(produto6);

		Carrinho carrinho1 = new Carrinho();
		carrinho1.addProduto(produto2, 10);
		carrinho1.addProduto(produto1, 20);
		carrinho1.addProduto(produto5, 24);
		carrinho1.orcamento();
		carrinho1.delProduto(1); // Remove o item 1 do carrinho
		carrinho1.orcamento();
		carrinho1.addProduto(produto6, 2);
		carrinho1.orcamento();
	}
}