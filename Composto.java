// Classe produto composto
class Composto extends Produto {
	private final int MAX_SIZE_ITENS = 100;
	protected float preco;
	private Simples[] itens;
	protected int itens_composicao;

	public Composto(){
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
		itens = new Simples[MAX_SIZE_ITENS];
		itens_composicao = 0;
	}
	public void exibeItens(){
		System.out.println("Código: " + this.getCodigo() + " Produto:" + this.nome + " Valor: " + String.format("%4.2f",this.sumPrice()));
		System.out.println("    Composicao");
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			System.out.println("    Item: " + (counter+1) + " " + this.itens[counter].getNome());
		}
	}
	// Adiciona item ao produto composto
	public void addItem(Simples value) {
		if (this.itens_composicao < MAX_SIZE_ITENS) {
			itens[this.itens_composicao] = value;
			this.itens_composicao++;
		}
	}
	// Remove item do produto composto
	public void delItem(int value){
		value--;
		if ((value < 0) || (value > this.itens_composicao)) {
			System.out.println("Não foi possível remover item da composicao");
			return;
		}
		System.out.println("Removendo: " + this.itens[value].nome);
		if (value == this.itens_composicao) {
			this.itens[this.itens_composicao] = null;
			this.itens_composicao--;
		} else {
			this.itens[value] = this.itens[this.itens_composicao-1];
			this.itens[this.itens_composicao-1] = null;
			this.itens_composicao--;
		}		
	}
	// Recupera preco de um item
	public float getPreco() {
		this.preco = 0F;
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			if (this.itens_composicao < MAX_SIZE_ITENS) {
				this.preco += itens[counter].getPreco();
			}
		}
		return this.preco;
	}
	// totaliza composicao
	private float sumPrice(){
		float acumulador = 0F;
		for (int counter = 0; counter < this.itens_composicao; counter++) {
			acumulador += this.itens[counter].getPreco();
		}
		return acumulador;		
	}
	public String toString(){
		return "Código: " + getCodigo() + " Produto: " + getNome() + " Preço: R$ " + String.format("%4.2f",getPreco());
	}

} // End of Composto class