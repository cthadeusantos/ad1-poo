//
// Auxiliary Class
// Métodos removidos da classe abstrata
abstract public class Produto {
	private static int proximo = 1;
	protected int codigo; // código do produto
	protected String nome;
	// Construtor
	Produto(){
		this.codigo = proximo++;
	}
	// Métodos definidos na classe abstrata
	// contém apenas o nome e o retorno da funcao
	//sem nenhuma implementação
	//
	// Define nome do produto
	public String setNome(String nome){
		this.nome = nome;
		return this.nome;
	}
	// Retorna nome do produto
	public String getNome(){
		return this.nome;
	}
	// Retorna código do produto
	public int getCodigo() {	
		return this.codigo;
	}

	// Retorna o preco
	abstract float getPreco();

	// Retorna codigo, nome e preco
	@Override
	public abstract String toString();
} // end of public class Produto