// Classe Produto
public class Simples extends Produto {
	// Aqui eu estava errando colocando private static
	// isso forcava preco a assumir o ultimo valor entrado em todas as instancias
	// foi so retirar que voltou ao normal ( entender o porquê disto acontecer )
	protected float preco;
	// Construtor
	public Simples() {
		// Segundo o livro Java Como programar - 10a edicao
		// Capitulo 9 Página 297
		// "Quando super-classe contiver um construtor sem argumento, pode-se utilizar super()
		// para chamá-lo em modo explicito, mas raramente isto é feito"
		super();
	}
	// Define preco
	public float setPreco(float preco){
		this.preco = preco;
		return this.preco;
	}
	// Recupera preco
	public float getPreco() {
		return this.preco;
    }
    public String toString(){
        return "Código: " + getCodigo() + " Produto: " + getNome() + " Preço: R$ " + String.format("%4.2f",getPreco());
}
} // End of Simples Class